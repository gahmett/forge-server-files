#!/bin/sh
. ./ayarlar.sh
eula_false() {
    grep -q 'eula=false' eula.txt
    return $?
}
if [ ! -f ${FORGEJAR} ]; then
  export URL="http://files.minecraftforge.net/maven/net/minecraftforge/forge/${FORGE_VERSION}/${FORGEINS}"
  wget -O ${FORGEINS} "${URL}"
  java -jar ${FORGEINS} --installServer
fi
if [ ! -f ${FORGEJAR} ]; then
    if [ -f ${FORGEJARX} ]; then
        cp ${FORGEJARX} ${FORGEJAR}
    fi
    start_server() {
        "$JAVACMD" -server -Xmx${MAX_RAM} ${JAVA_PARAMETERS} -jar ${FORGEJARX} nogui
    }
fi
if [ -f ${FORGEJAR} ]; then
start_server() {
    "$JAVACMD" -server -Xmx${MAX_RAM} ${JAVA_PARAMETERS} -jar ${FORGEJAR} nogui
}
fi
if [ ! -f ${JARFILE} -o ! -f libraries/${LAUNCHWRAPPER} ]; then
    echo "Missing required jars. Running install script!"
    sh ./install.sh
fi
if [ -f eula.txt ] && eula_false ; then
    echo "Make sure to read eula.txt before playing!"
    echo "To exit press <enter>"
    read ignored
    exit
fi
if [ ! -f eula.txt ]; then
    echo "Missing eula.txt. Startup will fail and eula.txt will be created"
    echo "Make sure to read eula.txt before playing!"
    echo "To continue press <enter>"
    read ignored
fi
echo "Starting server"
rm -f autostart.stamp
start_server
while [ -e autostart.stamp ] ; do
    rm -f autostart.stamp
    echo "If you want to completely stop the server process now, press Ctrl+C before the time is up!"
    for i in 5 4 3 2 1; do
        echo "Restarting server in $i"
        sleep 1
    done
    echo "Rebooting now!"
    start_server
    echo "Server process finished"
done