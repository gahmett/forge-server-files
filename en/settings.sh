cd "$(dirname "$0")"
. ./config.sh
export MCVER="1.16.5"
export JARFILE="minecraft_server.${MCVER}.jar"
export LAUNCHWRAPPERVERSION="1.16"
export LAUNCHWRAPPER="net/minecraft/launchwrapper/${LAUNCHWRAPPERVERSION}/launchwrapper-${LAUNCHWRAPPERVERSION}.jar"
export FORGEJAR="forge-${FORGE_VERSION}-universal.jar"
export FORGEJARX="forge-${FORGE_VERSION}.jar"
export FORGEINS="forge-${FORGE_VERSION}-installer.jar"
export JAVACMD="java"
export MAX_RAM="${RAM_VER}M"       # -Xmx
export JAVA_PARAMETERS="-XX:+UseParNewGC -XX:+CMSIncrementalPacing -XX:+CMSClassUnloadingEnabled -XX:ParallelGCThreads=5 -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10 -Duser.language=en -Duser.country=US -Dio.netty.leakDetection.level=advanced -Dfml.queryResult=confirm"