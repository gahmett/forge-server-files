#!/bin/sh
. ./ayarlar.sh
eula_false() {
    grep -q 'eula=false' eula.txt
    return $?
}
if [ ! -f ${FORGEJAR} ]; then
  export URL="http://files.minecraftforge.net/maven/net/minecraftforge/forge/${FORGE_VERSION}/${FORGEINS}"
  wget -O ${FORGEINS} "${URL}"
  java -jar ${FORGEINS} --installServer
fi
if [ ! -f ${FORGEJAR} ]; then
    if [ -f ${FORGEJARX} ]; then
        cp ${FORGEJARX} ${FORGEJAR}
    fi
    start_server() {
        "$JAVACMD" -server -Xmx${MAX_RAM} ${JAVA_PARAMETERS} -jar ${FORGEJARX} nogui
    }
fi
if [ -f ${FORGEJAR} ]; then
start_server() {
    "$JAVACMD" -server -Xmx${MAX_RAM} ${JAVA_PARAMETERS} -jar ${FORGEJAR} nogui
}
fi
if [ ! -f ${JARFILE} -o ! -f libraries/${LAUNCHWRAPPER} ]; then
    echo "Gerekli jar dosyalari eksik. Kurulum scripti calistiriliyor!"
    sh ./kurulum.sh
fi
if [ -f eula.txt ] && eula_false ; then
    echo "Oynamaya baslamadan once eula.txt dosyasini okudugunuzdan emin olun!"
    echo "Cikmak icin <enter> tusuna basiniz!"
    read ignored
    exit
fi
if [ ! -f eula.txt ]; then
    echo "eula.txt kayip. Baslatma hatali olacak ve eula.txt olusturulacak!"
    echo "Oynamaya baslamadan once eula.txt dosyasini okudugunuzdan emin olun!"
    echo "Cikmak icin <enter> tusuna basiniz!"
    read ignored
fi
echo "Server Baslatiliyor..."
rm -f autostart.stamp
start_server
while [ -e autostart.stamp ] ; do
    rm -f autostart.stamp
    echo "Sunucuyu simdi tamamen durdurmak istiyorsaniz, sure dolmadan Ctrl+C tuslarina basiniz!"
    for i in 5 4 3 2 1; do
        echo "Serveri yeniden baslatmak icin kalan sure : $i"
        sleep 1
    done
    echo "Simdi server yeniden baslatiliyor!"
    start_server
    echo "Server yeniden baslatildi!"
done
